import java.util.Scanner;
public class App{

	public static void main(String[] args) 
	{
		int N = input();
		first_cycle(N);
	}

	public static int input()
	{
		Scanner scanner = new Scanner(System.in);
		int N = scanner.nextInt();
		return N;	
	}

	public static void first_cycle(int N)
	{
		
		for(int i=2;i<=N;i++)
		{
		second_cycle(i);
		}
	}

	public static void second_cycle(int i)
	{
		int u=0;
		for(int k=2;k<=i;k++)
		{
			if(i%k==0)
			{				
				u=u+1;
			}
			if(u==1 && k==i)
			{
				output(i);
			}
		}
	}

	public static void output(int i)
	{
			System.out.println(i);
	}
}