import java.util.Scanner;
import java.util.Arrays;
public class FirstApp{

	public static void main(String[] args) 
	{
		String N = input();
		int k = check(N);
		if(k==1)
		{
			System.out.println("True");
		}
		else
		{
			System.out.println("False");
		}
	}

	public static String input()
	{
		Scanner scanner = new Scanner(System.in);
		String N = scanner.next();
		return N;	
	}

	public static Integer check(String N)
	{	
		String string = N;
		char[] chArray = string.toCharArray();
		int i = 0;
		for(int k=0;k<N.length()/2;k++)
		{
			i = N.length()-k-1;
			if(chArray[k]==chArray[i])
			{
				output(chArray[k]);
			}
			else
			{
				return 0;
			}
		}
		return 1;
	}

	public static void output(char i)
	{
			System.out.println(i);
	}
}