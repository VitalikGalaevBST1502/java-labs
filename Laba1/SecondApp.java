import java.util.Scanner;
import java.util.Arrays;
class SecondApp{

	public static void main(String[] args) 
	{
		System.out.println("Enter the number of the method");
		Scanner scanner = new Scanner(System.in);
		int N = scanner.nextInt();
		App applic = new App();
		FirstApp firstapplic = new FirstApp();
		switch(N)
		{
		case 1:
		{
			System.out.println("Enter a number to check");
			applic.start();	
			System.out.println("End");
			break;
		}
		case 2:
		{
			System.out.println("Enter a word to check");
			firstapplic.start();
			System.out.println("End");
			break;
		}
		}	
	}
}

class App
{

	public static void start() 
	{
		int N = input();
		first_cycle(N);
	}

	private static int input()
	{
		Scanner scanner = new Scanner(System.in);
		int N = scanner.nextInt();
		return N;	
	}

	private static void first_cycle(int N)
	{
		
		for(int i=2;i<=N;i++)
		{
		second_cycle(i);
		}
	}

	private static void second_cycle(int i)
	{
		int u=0;
		for(int k=2;k<=i;k++)
		{
			if(i%k==0)
			{				
				u=u+1;
			}
			if(u==1 && k==i)
			{
				output(i);
			}
		}
	}

	private static void output(int i)
	{
			System.out.println(i);
	}
}

class FirstApp{

	public static void start() 
	{
		String N = input();
		int k = check(N);
		if(k==1)
		{
			System.out.println("True");
		}
		else
		{
			System.out.println("False");
		}
	}

	private static String input()
	{
		Scanner scanner = new Scanner(System.in);
		String N = scanner.next();
		return N;	
	}

	private static Integer check(String N)
	{	
		String string = N;
		char[] chArray = string.toCharArray();
		int i = 0;
		for(int k=0;k<N.length()/2;k++)
		{
			i = N.length()-k-1;
			if(chArray[k]==chArray[i])
			{
				//output(chArray[k]);
			}
			else
			{
				return 0;
			}
		}
		return 1;
	}

	private static void output(char i)
	{
			System.out.println(i);
	}
}


