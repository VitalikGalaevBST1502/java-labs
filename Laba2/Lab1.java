import java.util.Scanner;
import java.util.Arrays;
import java.io.*;

class Lab1
{

	public static void main(String[] args) 
	{
		System.out.println("Enter x, y, z for the first point");
		double x1 = input(); 
		double y1 = input();
		double z1 = input();
		Point3d point1 = new Point3d(x1,y1,z1);
		System.out.println("Enter x, y, z for the second point");
		double x2 = input(); 
		double y2 = input();
		double z2 = input();
		Point3d point2 = new Point3d(x2,y2,z2);
		System.out.println("Enter x, y, z for the third point");
		double x3 = input(); 
		double y3 = input();
		double z3 = input();
		Point3d point3 = new Point3d(x3,y3,z3);
		if (point1.check(point2) || point1.check(point3) || point2.check(point3))
		{
			System.out.println("Error point");
		}
		else
		{
			double s=computeArea(point1.distanceTo(point2),point2.distanceTo(point3),point1.distanceTo(point3));
			output(s);
		}
	}
    public static double input()
    {
    	Scanner scanner1 = new Scanner(System.in);
		double N1 = scanner1.nextDouble();
		return N1;	
   	}

   	public static double computeArea(double x,double y,double z)
   	{
   		double P=(x+y+z)/2;
   		double S=Math.sqrt(P*(P-x)*(P-y)*(P-z));
//   	System.out.println(S);
   		return S;
//   		S = √(p·(p - a)·(p - b)·(p - c))
   	}

  	private static void output(double i)
	{
		System.out.println(i);
	}
   	
}
