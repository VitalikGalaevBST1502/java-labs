import java.util.Scanner;
import java.io.*;

class Lab1
{

	public static void main(String[] args) 
	{	
		//Создания объекта point1 класса Point3d
		Point3d point1 = new Point3d();
		System.out.println("Enter x, y, z for the first point");
		//Ввод значения x для point1
		point1.setX(input());
		//Ввод значения y для point1
		point1.setY(input());
		//Ввод значения z для point1
		point1.setZ(input());
		//Создания объекта point2 класса Point3d
		Point3d point2 = new Point3d();
		System.out.println("Enter x, y, z for the second point");
		//Ввод значения x для point2
		point2.setX(input());
		//Ввод значения y для point2
		point2.setY(input());
		//Ввод значения z для point2
		point2.setZ(input());
		//Создания объекта point3 класса Point3d
		Point3d point3 = new Point3d();
		System.out.println("Enter x, y, z for the third point");
		//Ввод значения x для point3
		point3.setX(input());
		//Ввод значения y для point3
		point3.setY(input());
		//Ввод значения z для point3
		point3.setZ(input());
		//Проверка равенства точек, если равны, то выведет сообщение об ошибке, иначе программа продолжит работу
		if (point1.check(point2) || point1.check(point3) || point2.check(point3))
		{
			System.out.println("Error point");
		}
		//Выполнение расчётов площади 
		else
		{
			//Расчёт площади
			double s=computeArea(point1.distanceTo(point2),point2.distanceTo(point3),point1.distanceTo(point3));
			//Вывод площади
			output(s);
			System.out.println("Complete");
		}
	}

	//Метод ввода значений типа double для координат точек
    public static double input()
    {
    	Scanner scanner1 = new Scanner(System.in);
		double N1 = scanner1.nextDouble();
		return N1;	
   	}

   	//Метод расчёта площади по формуле Герона
   	public static double computeArea(double x,double y,double z)
   	{
   		//Расчёт полупериметра
   		double P=(x+y+z)/2;
   		//Расчёт площади по формуле Герона
   		double S=Math.sqrt(P*(P-x)*(P-y)*(P-z));
   		return S;
   	}

   	//Метод вывода данных на экран
  	private static void output(double i)
	{
		System.out.println(i);
	}
   	
}
