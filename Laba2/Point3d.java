import java.util.Scanner;
import java.util.Arrays;
public class Point3d {
	public static void main(String[] args) 
	{
		//double X = input();
		//double Y = input();
		//Point3d myPoint = new Point3d ();//создает точку в (0,0)
    	//Point3d myOtherPoint = new Point3d (X,Y);//создает точку в (5,3)
    	//myOtherPoint.getX();
    	//myOtherPoint.getY();
   	//	Point3d aThirdPoint = new Point3d ();
	//	setX(X);
	//	setY(Y);
	//	output(xCoord);
	//	output(yCoord);
	}


	//public static double input()
	//{
	//	Scanner scanner = new Scanner(System.in);
	//	double N = scanner.nextDouble();
	//	return N;	
	//}

	//public static void output()
	//{
	//	System.out.println();
	//	System.out.println(i);
	//}
    /* X координат точки */
    private double xCoord;

    /* Y координата точки */
    private double yCoord;

	private double zCoord;
    /* Конструктор, чтобы инициализировать точку к (x, y) значение. */
    public Point3d(double x, double y, double z)
    {
        this.xCoord = x;
        this.yCoord = y;
        this.zCoord = z;
    }
/* Конструктор без параметров: значения по умолчанию к точке в источнике. */
//Вызовите конструктор с двумя параметрами и определите источник.
    public Point3d() 
    {
        this(0, 0, 0);
    }

    /* Верните X координат точки. */
    public double getX() 
    {
    	System.out.println(xCoord);
        return xCoord;
    }

    /* Возвратите координату Y точки. */
    public double getY() 
    {
    	System.out.println(yCoord);
        return yCoord;
    }

    /* Возвратите координату Z точки. */
    public double getZ() 
    {
    	System.out.println(zCoord);
        return zCoord;
    }

    /* Набор X координат точки. */
    public void setX(double val) 
    {
        this.xCoord = val;
    }

    /* Набор координата Y точки. */
    public void setY(double val) 
    {
        this.yCoord = val;
    }

    /* Набор координата Z точки. */
    public void setZ(double val) 
    {
        this.zCoord = val;
    }

    public void change(double a, String y)
    {
    	System.out.println("Select x or y or z");
    	switch(y)
    	{
    	case "x":
    	{
    	this.xCoord = a;	
    	}
    	case "y":
    	{
        this.yCoord = a;
        }
        case "z":
        {
        this.zCoord = a;
    	}
    	}
    }

    public boolean check(Point3d point)
    {
    	if(this.xCoord==point.xCoord && this.yCoord==point.yCoord && this.zCoord==point.zCoord)
    		{
    			return true;
    		}
    	else
    	{
			return false;
    	}
    }

    public double distanceTo(Point3d point)
    {
    	double Dist=Math.sqrt(Math.pow(point.xCoord-this.xCoord,2)+Math.pow(point.yCoord-this.yCoord,2)+Math.pow(point.zCoord-this.zCoord,2));
    	double roundOff = Math.round(Dist * 100.0) / 100.0;
    	return roundOff;
    }
}