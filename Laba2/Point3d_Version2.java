public class Point3d {
    /* X координат точки */
    private double xCoord;

    /* Y координата точки */
    private double yCoord;

    /* Z координата точки */
	private double zCoord;

    /* Конструктор, чтобы инициализировать точку к (x, y, z) значение. */
    public Point3d(double x, double y, double z)
    {
        this.xCoord = x;
        this.yCoord = y;
        this.zCoord = z;
    }
	/* Конструктор без параметров: значения по умолчанию к точке в источнике. */
	//Вызовите конструктор с тремя параметрами и определите источник.
    public Point3d() 
    {
        this(0, 0, 0);
    }

    /* Возвратите координату X точки. */
    public double getX() 
    {
        return xCoord;
    }

    /* Возвратите координату Y точки. */
    public double getY() 
    {
        return yCoord;
    }

    /* Возвратите координату Z точки. */
    public double getZ() 
    {
        return zCoord;
    }

    /* Набор координата X точки. */
    public void setX(double val) 
    {
        this.xCoord = val;
    }

    /* Набор координата Y точки. */
    public void setY(double val) 
    {
        this.yCoord = val;
    }

    /* Набор координата Z точки. */
    public void setZ(double val) 
    {
        this.zCoord = val;
    }

    //Данный метод измененяет координату точки
    public void change(double a, String y)
    {
    	System.out.println("Select x or y or z");
    	switch(y)
    	{
	    	case "x":
	    	{
	    	this.xCoord = a;	
	    	}
	    	case "y":
	    	{
	        this.yCoord = a;
	        }
	        case "z":
	        {
	        this.zCoord = a;
	    	}
		}
    }

    //Данный метод проверяет равенство двух точек и, если две точки равны, то возвращает true, иначе false
    public boolean check(Point3d point)
    {
    	if(this.getX()==point.getX() && this.getY()==point.getY() && this.getZ()==point.zCoord)
    		{
    			return true;
    		}
    	else
    	{
			return false;
    	}
    }

    //Данный метод считает расстояние между двумя точками
    public double distanceTo(Point3d point)
    {
    	double Dist=Math.sqrt(Math.pow(point.getX()-this.getX(),2)+Math.pow(point.getY()-this.getY(),2)+Math.pow(point.getZ()-this.getZ(),2));
    	double roundOff = Math.round(Dist * 100.0) / 100.0;
    	return roundOff;
    }
}